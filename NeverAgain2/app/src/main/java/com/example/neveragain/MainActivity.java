package com.example.neveragain;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;

public class MainActivity extends AppCompatActivity {

    private RadioButton opcionSiempre1, opcionMAmenudo1, opcionAmenudo1, opcionAveces1, opcionRaramente1, opcionNunca1;
    private RadioButton opcionSiempre2, opcionMAmenudo2, opcionAmenudo2, opcionAveces2, opcionRaramente2, opcionNunca2;
    private RadioButton opcionSiempre3, opcionMAmenudo3, opcionAmenudo3, opcionAveces3, opcionRaramente3, opcionNunca3;

    private Button siguiente;
    int respuesta1, respuesta2, respuesta3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        opcionSiempre1 = (RadioButton) findViewById(R.id.siempre1);
        opcionMAmenudo1 = (RadioButton) findViewById(R.id.amenudo1);
        opcionAmenudo1 = (RadioButton) findViewById(R.id.muyamenudo1);
        opcionAveces1 = (RadioButton) findViewById(R.id.algunasveces1);
        opcionRaramente1 = (RadioButton) findViewById(R.id.raramente1);
        opcionNunca1 = (RadioButton) findViewById(R.id.nunca1);

        opcionSiempre2 = (RadioButton) findViewById(R.id.siempre2);
        opcionMAmenudo2 = (RadioButton) findViewById(R.id.muyamenudo2);
        opcionAmenudo2 = (RadioButton) findViewById(R.id.amenudo2);
        opcionAveces2 = (RadioButton) findViewById(R.id.algunasveces2);
        opcionRaramente2 = (RadioButton) findViewById(R.id.raramente2);
        opcionNunca2 = (RadioButton) findViewById(R.id.nunca2);

        opcionSiempre3 = (RadioButton) findViewById(R.id.siempre1);
        opcionMAmenudo3 = (RadioButton) findViewById(R.id.muyamenudo3);
        opcionAmenudo3 = (RadioButton) findViewById(R.id.amenudo3);
        opcionAveces3 = (RadioButton) findViewById(R.id.algunasveces3);
        opcionRaramente3 = (RadioButton) findViewById(R.id.raramente1);
        opcionNunca3 = (RadioButton) findViewById(R.id.nunca1);

        siguiente = (Button) findViewById(R.id.siguiente1);
    }

    public void Siguiente (View view){
        Intent siguiente = new Intent(this, Main2Activity.class);
        startActivity(siguiente);
    }

}