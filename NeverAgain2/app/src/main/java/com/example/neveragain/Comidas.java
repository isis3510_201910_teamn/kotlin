package com.example.neveragain;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Comidas extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comidas);
    }


    public void ComidaDiaria (View view){
        Intent siguiente = new Intent(this, Comida.class);
        startActivity(siguiente);
    }

    public void Diario (View view){
        Intent siguiente = new Intent(this, Diario.class);
        startActivity(siguiente);
    }
    public void Peso (View view){
        Intent siguiente = new Intent(this, Inicio2.class);
        startActivity(siguiente);
    }
    public void Alarma (View view){
        Intent siguiente = new Intent(this, Inicio2.class);
        startActivity(siguiente);
    }

    public void Corazon (View view){
        Intent siguiente = new Intent(this, Motivacion.class);
        startActivity(siguiente);
    }

    public void Comida (View view) {
        Intent siguiente = new Intent(this, Comida.class);
        startActivity(siguiente);

    }

}
