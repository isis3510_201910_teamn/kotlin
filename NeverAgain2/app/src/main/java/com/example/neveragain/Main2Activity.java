package com.example.neveragain;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;

public class Main2Activity extends AppCompatActivity {

    private RadioButton opcionSiempre1, opcionMAmenudo1, opcionAmenudo1, opcionAveces1, opcionRaramente1, opcionNunca1;
    private RadioButton opcionSiempre2, opcionMAmenudo2, opcionAmenudo2, opcionAveces2, opcionRaramente2, opcionNunca2;
    private RadioButton opcionSiempre3, opcionMAmenudo3, opcionAmenudo3, opcionAveces3, opcionRaramente3, opcionNunca3;

    private Button siguiente;
    int respuesta1, respuesta2, respuesta3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        opcionSiempre1 = (RadioButton) findViewById(R.id.siempre4);
        opcionMAmenudo1 = (RadioButton) findViewById(R.id.amenudo4);
        opcionAmenudo1 = (RadioButton) findViewById(R.id.muyamenudo4);
        opcionAveces1 = (RadioButton) findViewById(R.id.algunasveces4);
        opcionRaramente1 = (RadioButton) findViewById(R.id.raramente4);
        opcionNunca1 = (RadioButton) findViewById(R.id.nunca4);

        opcionSiempre2 = (RadioButton) findViewById(R.id.siempre5);
        opcionMAmenudo2 = (RadioButton) findViewById(R.id.muyamenudo5);
        opcionAmenudo2 = (RadioButton) findViewById(R.id.amenudo5);
        opcionAveces2 = (RadioButton) findViewById(R.id.algunasveces5);
        opcionRaramente2 = (RadioButton) findViewById(R.id.raramente5);
        opcionNunca2 = (RadioButton) findViewById(R.id.nunca5);

        opcionSiempre3 = (RadioButton) findViewById(R.id.siempre6);
        opcionMAmenudo3 = (RadioButton) findViewById(R.id.muyamenudo6);
        opcionAmenudo3 = (RadioButton) findViewById(R.id.amenudo6);
        opcionAveces3 = (RadioButton) findViewById(R.id.algunasveces6);
        opcionRaramente3 = (RadioButton) findViewById(R.id.raramente6);
        opcionNunca3 = (RadioButton) findViewById(R.id.nunca6);

    }

    public void Siguiente (View view){
        Intent siguiente = new Intent(this, Main3Activity.class);
        startActivity(siguiente);
    }
    public void Anterior (View view){
        Intent anterior = new Intent(this, MainActivity.class);
        startActivity(anterior);
    }

}