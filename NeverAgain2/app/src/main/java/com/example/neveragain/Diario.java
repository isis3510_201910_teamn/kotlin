package com.example.neveragain;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Diario extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diario);
    }



    public void Comida (View view){
        Intent siguiente = new Intent(this, Comida.class);
        startActivity(siguiente);
    }
    public void Peso (View view){
        Intent siguiente = new Intent(this, Inicio2.class);
        startActivity(siguiente);
    }
    public void Alarma (View view){
        Intent siguiente = new Intent(this, Inicio2.class);
        startActivity(siguiente);
    }

    public void DiasFavoritos (View view){
        Intent siguiente = new Intent(this, DiasFavoritos.class);
        startActivity(siguiente);
    }

    public void AgregarDia (View view){
        Intent siguiente = new Intent(this, AgregarDia2.class);
        startActivity(siguiente);
    }

    public void ComidaFavorita (View view){
        Intent siguiente = new Intent(this, AgregarDia.class);
        startActivity(siguiente);
    }

    public void Corazon (View view){
        Intent siguiente = new Intent(this, Motivacion.class);
        startActivity(siguiente);
    }


}
