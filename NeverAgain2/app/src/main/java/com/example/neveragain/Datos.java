package com.example.neveragain;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Datos extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datos);
    }

    public void Siguiente (View view){
        Intent siguiente = new Intent(this, Motivacion.class);
        startActivity(siguiente);
    }
}
