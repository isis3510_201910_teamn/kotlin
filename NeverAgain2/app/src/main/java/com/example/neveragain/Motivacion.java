package com.example.neveragain;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Motivacion extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_motivacion);
    }


    public void Siguiente (View view){
        Intent siguiente = new Intent(this, Diario.class);
        startActivity(siguiente);
    }
}
